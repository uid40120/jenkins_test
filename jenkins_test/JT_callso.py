#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar 28 11:48:44 2021

@author: dom
"""

from ctypes import *

# In[]
so_file = "jenkins_test/hello.so"
my_functions = CDLL(so_file)

# In[]
print(type(my_functions))

# In[]
print(my_functions.printtests())
