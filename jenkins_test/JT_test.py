#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 26 23:07:41 2021

@author: dom
"""

# tests.py
from JT_callso import *

import random
try:
    import unittest2 as unittest
except ImportError:
    import unittest

class SimpleTest(unittest.TestCase):
    @unittest.skip("demonstrating skipping")
    def test_skipped(self):
        self.fail("shouldn't happen")

    def test_trivial(self):
        self.assertEqual(10, 10)

    def test_interface(self):
        self.assertEqual(my_functions.pythoninterface(3), 3)
        
        
if __name__ == '__main__':
    import xmlrunner
    unittest.main(testRunner=xmlrunner.XMLTestRunner(output='test-reports'))        
